import React from 'react';

const MyTasks = props => {

    return (
        <div style={{display: 'flex', alignSelf: 'start'}}>
            <p>{props.task}</p>
            <p>
                <button onClick={props.onRemove}>
                    Delete
                </button>
            </p>
        </div>
    );
};

export default MyTasks;