import {useState} from "react";
import {nanoid} from "nanoid";

import './App.css';
import AddTaskForm from "./addTaskForm/AddTaskForm";
import MyTasks from "./task/Task";

function App() {

    const [newTasks, setNewTasks] = useState([
        {task: 'Buy milk', id: nanoid()},
        {task: 'Walk with dog', id: nanoid()},
        {task: 'Do homework', id: nanoid()},
    ]);

    const [inputValue, setInputValue] = useState([
        {inputValue: 'Add new Task.js'}
    ]);


    const newInput = inputValue => {
        setInputValue({...inputValue, inputValue: inputValue});
    };


    const addTask = (task) => {
        setNewTasks([...newTasks, {
            task: task,
            id: nanoid(),
        }])
    };

    const removeTask = id => {
        setNewTasks(newTasks.filter(p => p.id !== id));
    };


    let tasksComponents = null;
    tasksComponents = newTasks.map(myTask => (
        <MyTasks key={myTask.id}
                 task={myTask.task}
                 onRemove={() => removeTask(myTask.id)}/>
    ));

    return (
        <div className="App"
             style={{
                 maxWidth: '750px',
                 margin: '0 auto',
                 display: 'flex',
                 flexDirection: 'column',
                 alignItems: 'center',
             }}>
            <AddTaskForm
                value={inputValue.inputValue}
                onAddNewTask={() => addTask(inputValue.inputValue)}
                onInputChange={e => newInput(e.target.value)}/>
            {tasksComponents}
        </div>
    );
}

export default App;
