import React from 'react';

const AddTaskForm = props => {

    return (
        <div style={{display: 'flex', alignSelf: 'start'}}>
            <p>
                <input type="text"
                       value={props.value}
                       onChange={props.onInputChange}
                />
            </p>
            <p>
                <button onClick={props.onAddNewTask}>Add</button>
            </p>
        </div>
    );
};

export default AddTaskForm;